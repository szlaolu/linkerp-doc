### 通用约定
1.访问域名：url  测试：http://192.168.22.144   正式：https://yunzhijia.com

2.调用方式：Post JSON

3.数据格式：

 - 参数：
```
{
    "key": "value"
}
示例：
{
    "eid": "10109"
}
```

 - 返回值：
```
{
    "data": "实际数据",
    "success": 是否成功  //true,false,
    "error": "错误信息， //成功时为空",
    "errorCode": 错误码， //成功时为0
}

示例：
{
    "data": "Ajl2Iobg0+OGjRMnSEzWGQ5RwhfHUMCMQWCU31d6F9s=",
    "success": true,
    "error": "鉴权信息数据获取失败!",
    "errorCode": 401
}
```

# OAUTH2.0授权接口
### 签名算法
使用sha1将参数使用ASCII 码从小到大排序（字典序）后生成签名
```java
Object[] list = new Object[]{"param1","param2","param3"};
Arrays.sort(list);
String sign = DigestUtils.shaHex(StringUtils.join(list));
```
例如接口请求参数为：{appId:xxxx,nonceStr:xxxx,signature:xxxx,timestamp:xxxx}
```java
String appId = "xxxx",secret = "xxxxx",nonceStr ="xxxx",timestamp = "xxxxx";
Object[] list = new Object[]{appId,secret,nonceStr,timestamp};
Arrays.sort(list);
String sign = DigestUtils.shaHex(StringUtils.join(list));
```
最终signature的值为例子中的sign
备注：接口中提到需要sha1签名的，除了signature参数不参与签名，其他参数都需要参与外，另外还需要加上应用的secret，签名结果即为signature参数的值

postMan签名方式：
在Pre-request Scripts中添加如下代码即可：
```javascript
var appId = "xxxxx",secret = "xxxx";
var data = {"appId":appId,"nonceStr":Math.random().toString(36).substr(2),"timestamp":Date.now()};
var list = [];
for(var k in data){
    list.push(data[k]);
}
list.push(secret);
list.sort();
data["signature"] = CryptoJS.SHA1(list.join("")).toString();
postman.setEnvironmentVariable("shenpidata",JSON.stringify(data));
```
在request body raw中加入以下内容，并将内容设置为application/json：
```
{{shenpidata}}
```

## 1、获取团队accessToken,该接口为企业自建应用获取团队accessToken
- url: /gateway/oauth2/token/getTeamAccessToken
- 参数：
```
{appId:xxxx,nonceStr:xxxx,signature:xxxx,timestamp:xxxx}
备注：signature由sha1签名算法得到，该接口为企业自建应用获取团队accessToken
```
- 返回值：
``` 
{
    "data": {
		accessToken:accessToken,
		expireIn:有效时间(秒),
		refreshToken:token刷新令牌
    },
    "errorCode": 0,
    "success": true
}

示例：
{
    "data": {
        "accessToken": "kuIpBlqyskChznZyJZq4A2ulCPVm2QPt",
        "expireIn": 7200,
        "refreshToken": "goaUXEEKYqzPzoSlXWTY03wrkx6FM9mQ"
    },
    "errorCode": 0,
    "success": true
}
```
## 2、刷新accessToken
- url: /gateway/oauth2/token/refreshToken
- 参数：
```
{appId:xxxx,refreshToken:xxx,nonceStr:xxxx,signature:xxxx,timestamp:xxxx}
备注：signature由sha1签名算法得到
```
- 返回值：
``` 
{
        "data": {
    		accessToken:accessToken,
    		expireIn:有效时间(秒),
    		refreshToken:token刷新令牌
        },
        "errorCode": 0,
        "success": true
}
示例：
{
     "data": {
        "accessToken": "kuIpBlqyskChznZyJZq4A2ulCPVm2QPt",
        "expireIn": 7200,
        "refreshToken": "goaUXEEKYqzPzoSlXWTY03wrkx6FM9mQ"
         },
        "errorCode": 0,
        "success": true
}
```

# API接口
## 1、批量保存门店信息
- url: /gateway/crm/store/saveStores
- 参数：
```
{
        "eid": "企业工作圈信息",   
        "data": [
                {
					"id":"ERP门店Id",
					"number":"ERP门店number",
					"name":"ERP门店name",
					"customer"：{"所属渠道F7结构体"},
					"storeType":{"门店类型F7结构体"},					
					"customerType":"渠道类型",
					"storeArea":"门店面积",					
					"cityType":"城市类型key,value",
					"phone":"门店电话",
					"address":"详细地址",
					"marketType":{"卖场类型F7结构体"},
					"marketName":"卖场名称",
					"province":"省",
					"city":"市",
					"district":"区",
					"cityNumber":"区号", 
					"longitude":"经度",
					"latitude":"纬度" 					
					"chargeUserOid": "负责人oid", 
				    "participates": ["参与人oid"]					 

				}  
        ]

}
示例：
{
    "eid": "2701576",
    "data": [
        {
            "id": "5be9466b7fef8b69734e484a",
            "number": "0755-000001",
            "name": "沃尔玛南山店",
            "customer": {
                "id": "5be9466b7fef8b69734e484a",
                "number": "0755-000001",
                "name": "沃尔玛南山店"
            },
            "storeType": {
                "id": "1",
                "number": "04",
                "name": "便利店"
            },
            "customerType": "现代渠道",
            "storeArea": "500米以下",
            "cityType": {
                "key": "a",
                "value": "A类城市"
            },
            "phone": "0755-86072332",
            "address": "广东省深圳市深圳市南山科技南区高新南十二路2号",
			 "marketType": {
                "id": "1",
                "number": "06",
                "name": "电商"
            },
            "marketName": "711",
            "province": "广东省",
            "city": "深圳市",
            "district": "南山区",
            "cityNumber": "0755",
            "longitude": "22.5588877511",
            "latitude": "11.55652877501",
            "chargeUserOid": "570f816800b0e953dc7af070",
            "participates": [
                "57331de300b0553cfe08813b",
                "570f816800b0e953dc7af070"
            ]
        }
    ]
}
```
- 返回值：
``` 
[
        {
           "id": "ERP门店id", 
		   "number": "ERP门店number",
           "storeId": "云之家门店id", 
           "error": "错误信息， //成功时为空",
           "errorCode": 错误码， //成功时为0
            
        }
]
示例：
[
		{
			"id": "5be9466b7fef8b69734e484a",
			"number": "0755-000001",
			"storeId": "1239466b7fef8b69734e4833",
			"error": null,
			"errorCode": 0
		}
]
```

## 2、批量禁用门店信息
- url: /gateway/crm/store/enableStores
- 参数：
```
{
        "eid": "企业工作圈信息",  
		"enable":"启用/禁用：true/false"	 		
        "data": [
                {
				    "id": "ERP门店id",
					"number":"ERP门店number"
				}  
        ]

}
示例：
{
    "eid": "10109",
	"enable":true,
    "data": [
        {
			"id": "5be9466b7fef8b69734e484a",
			"number": "0755-000001",
        }
    ]
}
```
- 返回值：
``` 
[
        {
		   "id": "ERP门店id",
           "number": "ERP门店number", 
		   "storeId": "云之家门店id", 
           "error": "错误信息， //成功时为空",
           "errorCode": 错误码， //成功时为0
            
        }
]
示例：
[
		{
			"id": "5be9466b7fef8b69734e484a",
			"number": "0755-000001",
			"storeId": "5bf60d157fef8b48e66e79d6",
			"error": null,
			"errorCode": 0
		}
]
```

## 3、批量保存客户信息
- url: /gateway/crm/customer/saveCustomers
- 参数：
```
{
        "eid": "企业工作圈信息",     
        "data": [
                {
					"id":"ERP渠道Id",
					"number":"ERP渠道number",
					"name":"ERP渠道name",
					"integratedNumber":"集成渠道number"
					"level":{"渠道等级F7结构体"},
					"fax":"公司传真",
					"phone":"公司电话",
					"province":"省",
					"city":"市",
					"district":"区",
					"address":"详细地址",					
					"chargeUserOid": "负责人oid", 
				    "participates": ["参与人oid"]					 

				}  
        ]

}
示例：
{
    "eid": "2701576",
    "data": [
        {
            "id": "5be9466b7fef8b69734e484a",
            "number": "0755-000001",
            "name": "沃尔玛南山店",
            "integratedNumber": "001069",
            "level": {
                "id": "1",
                "number": "01",
                "name": "一级"
            },
            "fax": "0755-86072332",
            "phone": "0755-86072332",
            "province": "广东省",
            "city": "深圳市",
            "district": "南山区",
            "address": "广东省深圳市深圳市南山科技南区高新南十二路2号",
            "chargeUserOid": "570f816800b0e953dc7af070",
            "participates": [
                "57331de300b0553cfe08813b",
                "570f816800b0e953dc7af070"
            ]
        }
    ]
}
```
- 返回值：
``` 
[
        {
           "id": "ERP渠道Id", 
		   "number": "ERP渠道number",
           "customerId": "云之家门店id", 
           "error": "错误信息， //成功时为空",
           "errorCode": 错误码， //成功时为0
            
        }
]
示例：
[
        {
            "number": "0755-000001",
            "customerId": "5bf55ea77fef8b4180d86ad5",
            "errorCode": 0,
            "id": "5be9466b7fef8b69734e484a",
            "error": ""
        }
]
```

## 4、批量禁用/启用客户信息
- url: /gateway/crm/customer/enableCustomers
- 参数：
```
{
        "eid": "企业工作圈信息",  
		"enable":"启用/禁用：true/false"	 		
        "data": [
                {
					"id": "ERP渠道Id", 
					"number":"ERP渠道number"
				}  
        ]

}
示例：
{
    "eid": "2701576",
    "enable":true,
    "data": [
        {
            "id": "5be9466b7fef8b69734e484a",
            "number": "0755-000001"
        }
    ]
}
```
- 返回值：
``` 
[
        {
		   "id": "ERP渠道Id", 
           "number": "ERP渠道number", 
		   "customerId": "云之家门店id",
           "error": "错误信息， //成功时为空",
           "errorCode": 错误码， //成功时为0
            
        }
]
示例：
[
		{
			"number": "0755-000001",
            "customerId": "5bf5616e7fef8b46657e5a85",
            "errorCode": 0,
            "id": "5be9466b7fef8b69734e484a",
            "error": ""
		}
]
```
