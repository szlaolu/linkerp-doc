### 通用约定
1.访问域名：url  测试：http://192.168.22.144   正式：https://yunzhijia.com

2.调用方式：Post JSON

3.数据格式：

 - 参数：
```
{
    "key": "value"
}
示例：
{
    "eid": "10109"
}
```

 - 返回值：
```
{
    "data": "实际数据",
    "success": 是否成功  //true,false,
    "error": "错误信息， //成功时为空",
    "errorCode": 错误码， //成功时为0
}

示例：
{
    "data": "Ajl2Iobg0+OGjRMnSEzWGQ5RwhfHUMCMQWCU31d6F9s=",
    "success": true,
    "error": "鉴权信息数据获取失败!",
    "errorCode": 401
}
```





## 1.创建分类
- url/linkerp/report/inner/biReport/createBiReportCategory

 - 参数：
```
{
        "eid": "企业工作圈信息",
        "oid": "创建人oid",        
        "name": "分类名称",  
        "authorizedOids": [
                "报表分类负责人oid"  
        ]//可为空，默认为创建人oid

}

示例：
{
        "eid": "2701576",    
        "oid": "570f816800b0e953dc7af070",
        "name": "人事类报表分类",  
        "authorizedOids": [
                "570f816800b0e953dc7af070","57331de300b0553cfe08813b"  
        ]

}
```
 - 返回值：
```
{"分类id"}

示例：
{"5b7cf65f8e82b480b4ac730c"}
```

异常说明：

| 错误码 | 错误信息 | 说明
| ------ | ------ | ------
| 1201002 | 名称为空 | 
| 1200000 | 云之家工作圈eid为空 | 
| 1200001 | 云之家用户oid为空 | 
| 1000021 | 数据读取异常 | 发生数据写入异常

    
## 2.获取分类
- url/linkerp/report/inner/biReport/getBiReportCategorys

- 参数：
```
{
        "eid": "企业工作圈信息",    
        "oid": "报表分类负责人oid"
}

示例：
{
        "eid": "10109",    
        "oid": "76ff620c-b8c3-11e3-8d13-e41f137ad9f4"
}
```

- 返回值：
```
[
        {
            "id": "报表分类ID",
            "name": "分类名称",
            "authorizedOids": [
                报表分类负责人oid
            ]
        }
]
        

示例：
        
 [
        {
            "name": "人事类报表分类",
            "id": "5b7cf65f8e82b480b4ac730c",
            "authorizedOids": [
                "570f816800b0e953dc7af070",
                "57331de300b0553cfe08813b"
            ]
        },
        {
            "name": "kingdee报表目录1",
            "id": "5b7bbe67b65a180b1711b069",
            "authorizedOids": [
                "570f816800b0e953dc7af070",
                "2c290b94-8f43-4259-813e-fe99667611c5",
                "3471f6f9-2abe-46de-bda8-e63baa32f137"
            ]
        },
        {
            "name": "财务类报表分类",
            "id": "5b768927b65a18228112330b",
            "authorizedOids": [
                "57331de300b0553cfe08813b",
                "570f816800b0e953dc7af070"
            ]
        },
        {
            "name": "销售订单报表",
            "id": "5b7637158e82b44cf4907b7f",
            "authorizedOids": [
                "570f816800b0e953dc7af070"
            ]
        }
]        
```

异常说明：

| 错误码 | 错误信息 | 说明
| ------ | ------ | ------
| 1200000 | 云之家工作圈eid为空 | 
| 1200001 | 云之家用户oid为空 | 


## 3.更新分类
- url/linkerp/report/inner/biReport/updateBiReportCategory

 - 参数：
```
{
        "eid": "企业工作圈信息",
        "oid": "创建人oid",    
        "biCategoryId":"分类id",        
        "name": "分类名称",  
        "authorizedOids": [
                "报表分类负责人oid"  
        ]

}

示例：
{
        "eid": "2701576",    
        "biCategoryId":"5b7d33bfb65a1845d9d7d71c",
        "oid": "570f816800b0e953dc7af070",
        "name": "人事类报表分类1",  
        "authorizedOids": [
                "570f816800b0e953dc7af070"
        ]

}
```
 - 返回值：
```
{
        "errorCode": 错误码,
        "success": 接口调用结果  //true,false
}
示例：
{
        "errorCode": 0,
        "success": true  
}
```

异常说明：

| 错误码 | 错误信息 | 说明
| ------ | ------ | ------
| 1200000 | 云之家工作圈eid为空 | 
| 1200001 | 云之家用户oid为空 | 
| 1201002 | 名称为空 | 
| 1201008 | 报表分类ID为空 | 
| 1201003 | 相关人员为空 | 报表分类负责人oid为空
| 1201013 | 不是分类负责人，无权操作 | 
| 1201006 | 找不到报表分类信息 | 报表分类不存在或已删除
| 1201004 | eid出错 | 工作圈eid与报表分类eid不符


## 4.删除分类
- url/linkerp/report/inner/biReport/delBiReportCategory

- 参数：
```
{
        "eid": "企业工作圈信息",    
        "oid": "报表分类负责人oid",
        "biCategoryId":"分类id"
}

示例：
{
        "eid": "2701576",    
        "oid": "570f816800b0e953dc7af070",
        "biCategoryId":"5b7cf65f8e82b480b4ac730c"
}
```
- 返回值：
```
{
        "errorCode": 错误码,
        "success": 接口调用结果  //true,false
}
示例：
{
        "errorCode": 0,
        "success": true  
}
```

异常说明：

| 错误码 | 错误信息 | 说明
| ------ | ------ | ------
| 1200000 | 云之家工作圈eid为空 | 
| 1200001 | 云之家用户oid为空 | 
| 1201008 | 报表分类ID为空 | 
| 1201013 | 不是分类负责人，无权操作 | 
| 1201006 | 找不到报表分类信息 | 报表分类不存在或已删除
| 1201004 | eid出错 | 工作圈eid与报表分类eid不符



## 5.创建报表
- url/linkerp/report/inner/biReport/createBiReport

- 参数：
```
{
         "eid": "企业工作圈信息",
         "oid": "创建者oid",
         "name": "报表名称", //最长20字符
         "reportCategoryId": "报表分类ID",
         "biId": "麦恰报表Id",  
          "responsibleUserOids": [
                "报表责任人oid列表”  
        ]
         "copyToUserOids": [
                "报表抄送人oid列表”  
        ],
        "pushTime":"推送时间", //格式为：1750表示17:50。为空则为立即推送。
        "remark": "备注说明" //可为空
      
}

示例：
{
        "eid": "2701576",    
        "oid": "570f816800b0e953dc7af070",
         "name": "销售订单报表",
         "reportCategoryId": "5b7cf65f8e82b480b4ac730c",
         "biId": "xxxxxxxxxxxx",  
          "responsibleUserOids":[
               "570f816800b0e953dc7af070"
        ],
         "copyToUserOids": [
               "570f816800b0e953dc7af070"  
        ],
        "pushTime":"1800",
        "remark": "取自ERP销售订单数据"
}
```
- 返回值：
```
{"biReportId":"报表id"}

示例：
{"biReportId":"5b7cfabf8e82b4719492fea7"}
```

异常说明：

| 错误码 | 错误信息 | 说明
| ------ | ------ | ------
| 1200000 | 云之家工作圈eid为空 | 
| 1200001 | 云之家用户oid为空 | 
| 1201002 | 名称为空 | 
| 1204001 | 报表名称过长 | 
| 1201008 | 报表分类ID为空 | 
| 1201057 | BI报表ID为空 | 
| 1201012 | 推送时间范围出错 | 
| 1201013 | 不是分类负责人，无权操作 | 
| 1201006 | 找不到报表分类信息 | 报表分类不存在或已删除
| 1201004 | eid出错 | 工作圈eid与报表分类eid不符

## 6.更新报表
- url/linkerp/report/inner/biReport/updateBiReport

- 参数：
```
{
         "eid": "企业工作圈信息",
         "oid": "创建者oid",
         "biReportId": "报表ID",
         "name": "报表名称", //最长20字符
          "responsibleUserOids": [
                "报表责任人oid列表”  
        ],
         "copyToUserOids": [
                "报表抄送人oid列表”  
        ]
      
}

示例：
{
         "eid": "50017220",
         "oid": "5b810bb184ae019ddf416db4",
         "biReportId": "5b83af4eb65a1854da3c3726",
         "name": "销售订单报表",
            "copyToUserOids": [
                "5b810dfce4b0c388915b44b2",
                "5b83a59fe4b0408f82450903",
                "5b83bc8de4b0408f82455d33"
            ],
            "responsibleUserOids": [
                "5b810bb184ae019ddf416db4"
            ]


}
```
 - 返回值：
```
{
        "errorCode": 错误码,
        "success": 接口调用结果  //true,false
}
示例：
{
        "errorCode": 0,
        "success": true  
}
```

异常说明：

| 错误码 | 错误信息 | 说明
| ------ | ------ | ------
| 1200000 | 云之家工作圈eid为空 | 
| 1200001 | 云之家用户oid为空 | 
| 1201002 | 名称为空 | 
| 1204001 | 报表名称过长 | 
| 1201008 | 报表分类ID为空 | 
| 1201057 | BI报表ID为空 | 
| 1201013 | 不是分类负责人，无权操作 | 
| 1201006 | 找不到报表分类信息 | 报表分类不存在或已删除
| 1202006 | 图表信息为空 | 
| 1201004 | eid出错 | 工作圈eid与报表分类eid不符

    
## 7.获取报表
- url/linkerp/report/inner/biReport/getBiReport

- 参数：
```
{
        "eid": "企业工作圈信息",    
        "oid": "报表分类负责人oid",
        "biReportId":"报表id"
}

示例：
{
        "eid": "2701576",    
        "oid": "570f816800b0e953dc7af070",
        "biReportId":"5b7cfabf8e82b4719492fea7"
}
```
- 返回值：
```
{
        "id": "报表id",
         "name": "报表名称",
         "reportCategoryId": "报表分类ID",
         "biId": "麦恰报表Id",  
          "responsibleUserOids": [
                "报表责任人oid列表”  
        ]
         "copyToUserOids": [
                "报表抄送人oid列表”  
        ]
        "pushTime":"推送时间",
        "remark": "备注说明",  
        "creatorOid": "创建者oid"
}        
        
示例：
{
        "id": "5b7cfabf8e82b4719492fea7",
         "name": "销售订单报表",
         "eid": "2701576",
        "copyToUserOids": [
            "570f816800b0e953dc7af070"
        ],
        "creatorOid": "570f816800b0e953dc7af070",
        "responsibleUserOids": [
            "570f816800b0e953dc7af070"
        ],
        "remark": "取自ERP销售订单数据",        
        "biId": "xxxxxxxxxxxx",
        "pushTime": 1800,
        "reportCategoryId": "5b7cf65f8e82b480b4ac730c"
}    
```

异常说明：

| 错误码 | 错误信息 | 说明
| ------ | ------ | ------
| 1200000 | 云之家工作圈eid为空 | 
| 1200001 | 云之家用户oid为空 | 
| 1201009 | 报表样式ID为空 | 
| 1202006 | 图表信息为空 | 
| 1201007 | 找不到报表主题信息 | 报表不存在或已删除
| 1201004 | eid出错 | 工作圈eid与报表分类eid不符


## 8.获取分类下所有报表
- url/linkerp/report/inner/biReport/getBiReportsByCategoryId

- 参数：
```
{
        "eid": "企业工作圈信息",    
        "oid": "报表分类负责人oid",
        "biCategoryId":"分类id"
}

示例：
{
        "eid": "2701576",    
        "oid": "570f816800b0e953dc7af070",
        "biCategoryId":"5b7cf65f8e82b480b4ac730c"
}
```
- 返回值：
```
[
    {
            "id": "报表id",
             "name": "报表名称",
             "reportCategoryId": "报表分类ID",
             "biId": "麦恰报表Id",  
              "responsibleUserOids": [
                    "报表责任人oid列表”  
            ]
             "copyToUserOids": [
                    "报表抄送人oid列表”  
            ]
            "pushTime":"推送时间",
            "remark": "备注说明",  
            "creatorOid": "创建者oid"
    }
]        
        
示例：
 [
        {
            "eid": "2701576",
            "copyToUserOids": [
                "570f816800b0e953dc7af070"
            ],
            "creatorOid": "570f816800b0e953dc7af070",
            "name": "销售订单报表",
            "responsibleUserOids": [
                "570f816800b0e953dc7af070"
            ],
            "remark": "取自ERP销售订单数据",
            "id": "5b7cfabf8e82b4719492fea7",
            "biId": "xxxxxxxxxxxx",
            "pushTime": 1800,
            "reportCategoryId": "5b7cf65f8e82b480b4ac730c"
        },
        {
            "eid": "2701576",
            "copyToUserOids": [
                "570f816800b0e953dc7af070"
            ],
            "creatorOid": "570f816800b0e953dc7af070",
            "name": "销售订单报表",
            "responsibleUserOids": [
                "570f816800b0e953dc7af070"
            ],
            "remark": "取自ERP销售订单数据",
            "id": "5b7cf8828e82b480b4ac7318",
            "biId": "xxxxxxxxxxxx",
            "pushTime": 1800,
            "reportCategoryId": "5b7cf65f8e82b480b4ac730c"
        },
        {
            "eid": "2701576",
            "copyToUserOids": [
                "570f816800b0e953dc7af070"
            ],
            "creatorOid": "570f816800b0e953dc7af070",
            "name": "销售订单报表",
            "responsibleUserOids": [
                "570f816800b0e953dc7af070"
            ],
            "remark": "取自ERP销售订单数据",
            "id": "5b7cf8478e82b480b4ac7315",
            "biId": "xxxxxxxxxxxx",
            "pushTime": 1800,
            "reportCategoryId": "5b7cf65f8e82b480b4ac730c"
        }
]
```

异常说明：

| 错误码 | 错误信息 | 说明
| ------ | ------ | ------
| 1200000 | 云之家工作圈eid为空 | 
| 1200001 | 云之家用户oid为空 | 
| 1201008 | 报表分类ID为空 | 
| 1201013 | 不是分类负责人，无权操作 | 
| 1201006 | 找不到报表分类信息 | 报表分类不存在或已删除
| 1201004 | eid出错 | 工作圈eid与报表分类eid不符

## 9.删除报表
- url/linkerp/report/inner/biReport/delBiReport

- 参数：
```
{
        "eid": "企业工作圈信息",
        "oid": "报表分类负责人oid",
        "biReportId":"报表id"
}
示例：
{
        "eid": "2701576",    
        "oid": "570f816800b0e953dc7af070",
        "biReportId": "5b76376a8e82b44cf4907b81"
}
```
 - 返回值：
```
{
        "errorCode": 错误码,
        "success": 接口调用结果  //true,false
}
示例：
{
        "errorCode": 0,
        "success": true  
}
```

异常说明：

| 错误码 | 错误信息 | 说明
| ------ | ------ | ------
| 1200000 | 云之家工作圈eid为空 | 
| 1200001 | 云之家用户oid为空 | 
| 1201009 | 报表样式ID为空 | 
| 1201007 | 找不到报表主题信息 | 
| 1201004 | eid出错 | 工作圈eid与报表分类eid不符
| 1201008 | 报表分类ID为空 | 
| 1201013 | 不是分类负责人，无权操作 | 
| 1201006 | 找不到报表分类信息 | 

## 10.生成报表数据
- url/linkerp/report/inner/biReport/generateBiReportData

- 参数：
```
{
        "eid": "企业工作圈信息",
        "oid": "报表分类负责人oid",
        "biReportId":"报表id",
         "base64String":"图片base64后的数据"
}
示例：
{
        "eid": "2701576",    
        "oid": "570f816800b0e953dc7af070",
        "biReportId": "5b76376a8e82b44cf4907b81",
         "base64String":"实际数据"
}
```
 - 返回值：
```
{
        "errorCode": 错误码,
        "success": 接口调用结果  //true,false
}
示例：
{
        "errorCode": 0,
        "success": true  
}
```

异常说明：

| 错误码 | 错误信息 | 说明
| ------ | ------ | ------
| 1200000 | 云之家工作圈eid为空 | 
| 1200001 | 云之家用户oid为空 | 
| 1201009 | 报表样式ID为空 | 
| 1201007 | 找不到报表主题信息 | 
| 1201004 | eid出错 | 工作圈eid与报表分类eid不符
| 1201008 | 报表分类ID为空 | 
| 1201013 | 不是分类负责人，无权操作 | 
| 1201006 | 找不到报表分类信息 | 



